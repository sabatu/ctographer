﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualizationEngine
{
   public class GraphContent
    {
        //public Texture2D RedLaser { get; set; }     
        public Texture2D GraphBackground { get; set; }

        public SpriteFont ClassFont { get; set; }

        public GraphContent(ContentManager Content)
        {
            //load images

            //imgPixel = Content.Load<Texture2D>("Pixel");

            //load fonts
            ClassFont = Content.Load<SpriteFont>("ClassText");

        }
    }
}
