﻿using System.Collections.Generic;
using VisualizationEngine.Models.Class_Components;

namespace VisualizationEngine.Controllers
{
    public class ProgramController
    {
        //For now, this will have to be the example class.
        private string rawClassMetadata = System.IO.File.ReadAllText(@"C:\Users\Sabatu\Desktop\CTographerFiles\ship.cs ");
        private ClassController _classCon = new ClassController();


        public CSharpSystem ReturnSystemData()
        {
            List<Variable> classVariables = new List<Variable>();
            List<Method> classMethods = new List<Method>();     

            List<string> classVariableData = _classCon.ParseVariables(rawClassMetadata);

            foreach(string classVariable in classVariableData)
            {
                classVariables.Add(new Variable() { Name = classVariable });
            }

            List<string> classMethodData = _classCon.ParseMethods(rawClassMetadata);

            foreach(string methodVariable in classMethodData)
            {
                classMethods.Add(new Method() { MethodName = methodVariable });
            }

            string className = _classCon.ParseClass(rawClassMetadata);
            List<string> classDependencies = _classCon.ParseIncludes(rawClassMetadata);
            string classNamespace = _classCon.ParseNamespace(rawClassMetadata);

            Class classData = new Class() { ClassNamespace = classNamespace,
                                            ClassDependencies = classDependencies,
                                            ClassDescriptor = className,
                                            ClassMethods = classMethods,
                                            ClassVariables = classVariables };


            return new CSharpSystem()
            {
                SystemClasses = new List<Class>() { classData }
            };
        }



    }
}
