﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using VisualizationEngine.Models.Enums;
namespace VisualizationEngine.Controllers
{
    public class ClassController
    {       

        //Match expressions for access modifiers, or type names.
        private string accessMatch = string.Join("|", Enum.GetNames(typeof(CAccessModifiers)));
        private string typeMatch = string.Join("|", Enum.GetNames(typeof(CPrimitiveTypes)));

        public List<string> ParseVariables(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex variableDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}(" + @"\s" + "){1}(" + typeMatch + "){1}(" + @"\s" + "){1}([\\w])+([^()])+$", RegexOptions.IgnorePatternWhitespace);
            return rawClassLines.Where(accessModLine => variableDeclarationsRegex.IsMatch(accessModLine)).ToList();
        }

        public List<string> ParseMethods(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex methodDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}" + @"\s" + "(" + typeMatch + "){1}(" + @"\s" + "){1}([\\w])+([()])+$");
            return rawClassLines.Where(accessModLine => methodDeclarationsRegex.IsMatch(accessModLine)).ToList(); //Consider that this will need to be eventually updated to work with user-defined types.
        }

        public string ParseClass(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex classDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}(" + @"\s" + "){1}(class){1}(" + @"\s" + "){1}([\\w])+([^()])+$");
            return rawClassLines.Where(accessModLine => classDeclarationsRegex.IsMatch(accessModLine)).FirstOrDefault();
        }

        public string ParseNamespace(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex namespaceDeclarationRegex = new Regex("^" + "(" + @"\s" + ")*" + "(namespace){1}(" + @"\s" + "){1}([\\w.])+$");
            return rawClassLines.Where(accessModLine => namespaceDeclarationRegex.IsMatch(accessModLine)).FirstOrDefault();
        }

        public List<string> ParseIncludes(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex includeClauseRegex = new Regex("^" + "(" + @"\s" + ")*" + "(using){1}(" + @"\s" + "){1}([\\w.;])+$");
            return rawClassLines.Where(accessModLine => includeClauseRegex.IsMatch(accessModLine)).ToList();
        }

   
        public void ReturnClassFile(int classFileID)
        {
            
        }
    }
}
