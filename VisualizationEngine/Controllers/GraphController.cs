﻿

using CTographer.Controllers;
using CTographer.Models.Class_Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualizationEngine.Interfaces;
using VisualizationEngine.Models;


namespace VisualizationEngine.Controllers
{
    public class GraphController
    {
        public ProgramController _programCon = new ProgramController();

        public IEnumerable<IGraphSymbol> GraphSystemData()
        {
            CSharpSystem systemData = _programCon.ReturnSystemData();
            Class systemClass = systemData.SystemClasses.First();

            List<IGraphSymbol> classFileSymbols = new List<IGraphSymbol>();

            List<ClassSymbol> variableSymbols =  ConstructVariables(systemClass.ClassVariables);
            classFileSymbols.AddRange(variableSymbols);

            //Determine maximum horizontal right boundary of variables - this will be used to display methods as appropriate.
            int variableSpacer = variableSymbols.Max(variableNode => variableNode.RightBoundary);
            List<ClassSymbol> methodSymbols = ConstructMethods(systemClass.ClassMethods, variableSpacer);
            classFileSymbols.AddRange(methodSymbols);

            int methodSpacer = methodSymbols.Max(methodNode => methodNode.RightBoundary);
            List<ClassSymbol> classSymbol = ConstructClasses(new List<Class>() { systemClass }, methodSpacer);
            classFileSymbols.AddRange(classSymbol);            

            List<GraphLine> graphLines = ConstructGraphLines(classSymbol.First(), methodSymbols, variableSymbols);
            classFileSymbols.AddRange(graphLines);

            return classFileSymbols;
        }

        public List<ClassSymbol> ConstructVariables(List<Variable> variables)
        {
            List<ClassSymbol> variableNodes = new List<ClassSymbol>();
            
            int absoluteXPosition = 100;
            int absoluteYPosition = 100;

            List<string> variableNames = variables.Select(variable => variable.Name).ToList();

            foreach (string variableName in variableNames)
            {
                variableNodes.Add(new ClassSymbol() { Name = variableName,
                                                          NameColor = Color.Red,
                                                          SymbolicType = SymbolType.Variable,
                                                          DisplayPositionX = absoluteXPosition,
                                                          DisplayPositionY = absoluteYPosition });
                absoluteYPosition += 40;
            }

            return variableNodes;
        }

        public List<ClassSymbol> ConstructMethods(List<Method> methods, int variableSpacer)
        {
            List<ClassSymbol> methodNodes = new List<ClassSymbol>();

            int absoluteXPosition = variableSpacer;
            int absoluteYPosition = 270;

            List<string> methodNames = methods.Select(method => method.MethodName).ToList();

            foreach (string methodName in methodNames)
            {
                methodNodes.Add(new ClassSymbol() { Name = methodName,
                                                        NameColor = Color.Blue,
                                                        SymbolicType = SymbolType.Method,
                                                        DisplayPositionX = absoluteXPosition,
                                                        DisplayPositionY = absoluteYPosition });
                absoluteYPosition += 40;
            }

            return methodNodes;
        }

        public List<ClassSymbol> ConstructClasses(List<Class> classes, int methodSpacer)
        {
            List<ClassSymbol> classNodes = new List<ClassSymbol>();

            int absoluteXPosition = methodSpacer + 700;
            int absoluteYPosition = 320;

            List<string> classNames = classes.Select(classFile => classFile.ClassDescriptor).ToList();

            foreach (string className in classNames)
            {
                classNodes.Add(new ClassSymbol()
                {
                    Name = className,
                    NameColor = Color.Black,
                    SymbolicType = SymbolType.Class,
                    DisplayPositionX = absoluteXPosition,
                    DisplayPositionY = absoluteYPosition
                });
                absoluteYPosition += 40;
            }

            return classNodes;
        }

        public List<GraphLine> ConstructGraphLines(ClassSymbol classSymbol, List<ClassSymbol> methods, List<ClassSymbol> variables)
        {
            Color lineColor = Color.SlateGray;
            List<GraphLine> graphLines = new List<GraphLine>();

            int variablesStartX = variables.First().RightBoundary;
            int variablesStartY = variables.Min(x => x.DisplayPositionY) - 5;

            int variablesEndX = methods.First().DisplayPositionX - 5;
            int variablesEndY = methods.Min(x => x.DisplayPositionY) + 5;

            graphLines.Add(new GraphLine(variablesStartX, variablesStartY, variablesEndX, variablesEndY, lineColor));

            int variablesLowerBoundY = variables.Max(x => x.DisplayPositionY) + 5;
            int methodsEndY = methods.Max(x => x.DisplayPositionY) + 5;

            graphLines.Add(new GraphLine(variablesStartX, variablesLowerBoundY, variablesEndX, methodsEndY, lineColor));

            int methodStartX = methods.First().DisplayPositionX + 250;
            int methodStartY = methods.Min(x => x.DisplayPositionY) + 50;

            int methodEndX = classSymbol.DisplayPositionX - 20;
            int MethodEndY = methodStartY;

            graphLines.Add(new GraphLine(methodStartX, methodStartY, methodEndX, MethodEndY, lineColor));

            return graphLines;
        }
    }
}
