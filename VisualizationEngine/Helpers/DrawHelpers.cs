﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualizationEngine.Helpers
{
    public static class DrawHelpers
    {
       
        public static void DrawLine(SpriteBatch sb, Vector2 start, Vector2 end, GraphicsDevice grapher, Color lineColor)
        {
            Texture2D lineTexture = new Texture2D(grapher, 1, 1); //base for the line texture
            lineTexture.SetData<Color>(
                new Color[] { Color.White });// fill the texture with white

            Vector2 edge = end - start;
            // calculate angle to rotate line
            float angle =
                (float)Math.Atan2(edge.Y, edge.X);


            sb.Draw(lineTexture,
                new Rectangle(// rectangle defines shape of line and position of start of line
                    (int)start.X,
                    (int)start.Y,
                    (int)edge.Length(), //sb will stretch the texture to fill this rectangle
                    1), //width of line, change this to make thicker line
                null,
                lineColor, //colour of line
                angle,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);

        }
    }
}
