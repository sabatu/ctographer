﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualizationEngine.Interfaces;

namespace VisualizationEngine.Models
{
    public class ClassSymbol : IGraphSymbol
    {
        public string Name { get; set; }

        public string DisplayName
        {
            get { return CalculateDisplayName(); }
        }
        public SymbolType SymbolicType { get; set; }
        public int? LineNumber { get; set; }
        public int DisplayPositionX { get; set; }
        public int DisplayPositionY { get; set; }
        public Color NameColor { get; set; }

        //This is an average taken from testing. Useful for figuring out where to place lines in relation to different nodes.
        private int PixelsPerCharacter = 7;

        public int RightBoundary
        {
            get { return CalculateRightBoundary(); }
        }



        public ClassSymbol()
        {

        }

        public ClassSymbol(string name, int xPosition, int yPosition, Color nameColor, SymbolType symType)
        {
            this.Name = name;
            this.DisplayPositionX = xPosition;
            this.DisplayPositionY = yPosition;
            this.NameColor = nameColor;
            this.SymbolicType = symType;
        }

        public int CalculateRightBoundary()
        {
            return DisplayPositionY + (DisplayName.Length * PixelsPerCharacter);
        }

        public string CalculateDisplayName()
        {
            return SymbolicType + ":" + Name;
        }

        public void Draw()
        {
            Globals.Instance.spriteBatch.DrawString(Globals.Instance.GameContent.ClassFont,
                                this.DisplayName,
                                new Vector2(this.DisplayPositionX, // Horizontal Spacing (from left.)
                                this.DisplayPositionY), //Vertical Spacing (from top.)
                               this.NameColor);
        }


    }
}
