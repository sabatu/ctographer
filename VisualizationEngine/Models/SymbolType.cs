﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualizationEngine.Models
{
    public enum SymbolType
    {
        Variable = 0,
        Method = 1,
        Class = 2
    }
}
