﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualizationEngine.Helpers;
using VisualizationEngine.Interfaces;

namespace VisualizationEngine.Models
{
    public class GraphLine :IGraphSymbol
    {
        public int StartPositionX { get; set; }
        public int StartPositionY { get; set; }
        public int EndPositionX { get; set; }
        public int EndPositionY { get; set; }
        public Color LineColor { get; set; }

        public GraphLine()
        {

        }

        public GraphLine(int xStartPosition, int yStartPosition, int xEndPosition, int yEndPosition, Color lineColor)
        {
     
            this.StartPositionX = xStartPosition;
            this.StartPositionY = yStartPosition;
            this.EndPositionX = xEndPosition;
            this.EndPositionY = yEndPosition;
            this.LineColor = lineColor;
        }

        public void Draw()
        {
            DrawHelpers.DrawLine(Globals.Instance.spriteBatch, //draw line
                    new Vector2(StartPositionX, StartPositionY), //start of line
                    new Vector2(EndPositionX, EndPositionY), //end of line
                    Globals.Instance.GraphicsDevice, LineColor
);
        }
    }
}
