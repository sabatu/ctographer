﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualizationEngine
{
    public class GraphBackground
    {
        public float Width { get; set; } //width of graph
        public float Height { get; set; } //height of graph

        private Texture2D background { get; set; }  //cached image single pixel we'll use to draw the border lines
        private SpriteBatch spriteBatch;  //allows us to write on backbuffer when we need to draw self

        public GraphBackground()
        {
            Width = Globals.Instance.ScreenWidth;
            Height = Globals.Instance.ScreenHeight;
            background = Globals.Instance.GameContent.GraphBackground;
            this.spriteBatch = Globals.Instance.spriteBatch;
        }

        public void Draw()
        {
            //spriteBatch.Draw(background, new Rectangle(0, 0, 800, 800), Color.White);  //canvas map with static image for now
        }




    }
}
