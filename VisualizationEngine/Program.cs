﻿using System;

namespace VisualizationEngine
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new UserScreen())
                game.Run();
        }
    }
}
