﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using VisualizationEngine.Controllers;
using VisualizationEngine.Helpers;
using VisualizationEngine.Interfaces;
using System.Linq;

namespace VisualizationEngine
{    
    public class UserScreen : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private GraphBackground graphBackground;

        private MouseState oldMouseState;
        private KeyboardState oldKeyboardState;       

        //Custom timing values.
        private int GameTimerSeconds = 0;
        private int GameTimerMilliSeconds = 0;

        private int InstanceResetSeconds = 0;
        private int InstanceResetMilliseconds = 0;

        public UserScreen()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Globals.Instance.GameContent = new GraphContent(Content);
            // Create a new SpriteBatch, which can be used to draw textures.
            Globals.Instance.spriteBatch = new SpriteBatch(GraphicsDevice);       
            Globals.Instance.GameContent = new GraphContent(Content);


            Globals.Instance.ScreenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Globals.Instance.ScreenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            Globals.Instance.GraphicsDevice = GraphicsDevice;
            //set game to 502x700 or screen max if smaller
            //if (Globals.Instance.ScreenWidth >= 502)
            //{
            //    Globals.Instance.ScreenWidth = 502; 
            //}
            //if (Globals.Instance.ScreenHeight >= 700)
            //{
            //    Globals.Instance.ScreenHeight = 700;
            //}
            graphics.PreferredBackBufferWidth = Globals.Instance.ScreenWidth;
            graphics.PreferredBackBufferHeight = Globals.Instance.ScreenHeight;
            graphics.ApplyChanges();

            graphBackground = new GraphBackground();

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            KeyboardState newKeyboardState = Keyboard.GetState();
            MouseState newMouseState = Mouse.GetState();

            if (newMouseState.LeftButton == ButtonState.Released && oldMouseState.LeftButton == ButtonState.Pressed && oldMouseState.X == newMouseState.X && oldMouseState.Y == newMouseState.Y)
            {
                
            }

            if (newKeyboardState.IsKeyDown(Keys.Space))
            {
                
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            GraphicsDevice.Clear(Color.LightGray);  //Change the GraphicsDevice.Clear method to use Color.Black as shown here          

            //Keep instance timers set to MonoGame timer unless conditional logic changes.
            GameTimerSeconds = (((gameTime.TotalGameTime.Minutes * 60) + gameTime.TotalGameTime.Seconds) - InstanceResetSeconds);
            GameTimerMilliSeconds = (gameTime.TotalGameTime.Milliseconds - InstanceResetMilliseconds);


            Globals.Instance.spriteBatch.Begin();


            spriteBatch.Begin();

            //Controllers draw here.
            GraphController _graphCon = new GraphController();
            List<IGraphSymbol> graphingNodes = (_graphCon.GraphSystemData()).ToList();

            foreach(IGraphSymbol classSymbol in graphingNodes)
            {
                classSymbol.Draw();
            }

            spriteBatch.End();

            Globals.Instance.spriteBatch.End();


            graphBackground.Draw();

            base.Draw(gameTime);
        }


    }
}
