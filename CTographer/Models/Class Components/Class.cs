﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTographer.Models.Class_Components
{
    public class Class
    {
        public string ClassDescriptor;
        public List<Method> ClassMethods;
        public List<Variable> ClassVariables;
        public string ClassNamespace;
        public List<string> ClassDependencies;
        public Class()
        {

        }
    }
}
