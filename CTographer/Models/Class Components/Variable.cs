﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTographer.Models.Class_Components
{
    public class Variable
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public Type VType { get; set; }
    }
}
