﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTographer.Models.Enums
{
    public enum CAccessModifiers
    {
        @public = 0,
        @private = 1,
        @protected = 2
    }
}
