﻿using CTographer.Models.Class_Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTographer.Models
{
    public class ClassFile
    {
        public ClassFile()
        {
            this.ClassMethods = new List<Method>();
            this.ClassVariables = new List<Variable>();
            this.IncludedNamespaces = new List<string>();
        }
        public string ClassName { get; set; }
        public List<Method> ClassMethods { get; set; }
        public List<Variable> ClassVariables { get; set; }
        public List<string> IncludedNamespaces { get; set; }

    }
}
