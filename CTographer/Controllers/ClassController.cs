﻿using System;
using System.Collections.Generic;
using System.Text;
using CTographer.Models;
using System.Linq;
using CTographer.Models.Enums;
using System.Text.RegularExpressions;

namespace CTographer.Controllers
{
    public class ClassController
    {       

        //Match expressions for access modifiers, or type names.
        private string accessMatch = string.Join("|", Enum.GetNames(typeof(CAccessModifiers)));
        private string typeMatch = string.Join("|", Enum.GetNames(typeof(CPrimitiveTypes)));

        public List<string> ParseVariables(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex variableDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}(" + @"\s" + "){1}(" + typeMatch + "){1}(" + @"\s" + "){1}([\\w])+([^()])+$", RegexOptions.IgnorePatternWhitespace);
            return rawClassLines.Where(accessModLine => variableDeclarationsRegex.IsMatch(accessModLine)).ToList();
        }

        public List<string> ParseMethods(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex methodDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}" + @"\s" + "(" + typeMatch + "){1}(" + @"\s" + "){1}([\\w])+([()])+$");
            return rawClassLines.Where(accessModLine => methodDeclarationsRegex.IsMatch(accessModLine)).ToList(); //Consider that this will need to be eventually updated to work with user-defined types.
        }

        public string ParseClass(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex classDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}(" + @"\s" + "){1}(class){1}(" + @"\s" + "){1}([\\w])+([^()])+$");
            return rawClassLines.Where(accessModLine => classDeclarationsRegex.IsMatch(accessModLine)).FirstOrDefault();
        }

        public string ParseNamespace(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex namespaceDeclarationRegex = new Regex("^" + "(" + @"\s" + ")*" + "(namespace){1}(" + @"\s" + "){1}([\\w.])+$");
            return rawClassLines.Where(accessModLine => namespaceDeclarationRegex.IsMatch(accessModLine)).FirstOrDefault();
        }

        public List<string> ParseIncludes(string rawClassText)
        {
            string[] rawClassLines = rawClassText.Replace("\n", String.Empty).Split('\r');
            Regex includeClauseRegex = new Regex("^" + "(" + @"\s" + ")*" + "(using){1}(" + @"\s" + "){1}([\\w.;])+$");
            return rawClassLines.Where(accessModLine => includeClauseRegex.IsMatch(accessModLine)).ToList();
        }





        public void SaveClassFile()
        {
            //Grab raw code from class file.
             string rawClassMetadata = System.IO.File.ReadAllText(@"C:\Users\Sabatu\Desktop\CTographerFiles\ship.cs ");

            //Remove newlines, separate lines by carriage returns '\r'.
             string[] rawClassLines = rawClassMetadata.Replace("\n", String.Empty).Split('\r');

            List<string> typeLineMatches = new List<string>();



            ClassFile inputClass = new ClassFile();          


            Regex variableDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}(" + @"\s" + "){1}(" + typeMatch + "){1}(" + @"\s" + "){1}([\\w])+([^()])+$", RegexOptions.IgnorePatternWhitespace);
            Regex methodDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}" + @"\s" + "(" + typeMatch + "){1}(" + @"\s" + "){1}([\\w])+([()])+$");
            Regex classDeclarationsRegex = new Regex("^" + "(" + @"\s" + ")*" + "(" + accessMatch + "){1}(" + @"\s" + "){1}(class){1}(" + @"\s" + "){1}([\\w])+([^()])+$");
            Regex includeClauseRegex = new Regex("^" + "(" + @"\s" + ")*" + "(using){1}(" + @"\s" + "){1}([\\w.;])+$");
            Regex namespaceDeclarationRegex = new Regex("^" + "(" + @"\s" + ")*" + "(namespace){1}(" + @"\s" + "){1}([\\w.])+$");
            

            List<string> variableDeclarationLines = rawClassLines.Where(accessModLine => variableDeclarationsRegex.IsMatch(accessModLine)).ToList();            
            List<string> methodDeclarationLines = rawClassLines.Where(accessModLine => methodDeclarationsRegex.IsMatch(accessModLine)).ToList(); //Consider that this will need to be eventually updated to work with user-defined types.
            string classDeclarationLine = rawClassLines.Where(accessModLine => classDeclarationsRegex.IsMatch(accessModLine)).FirstOrDefault();
            List<string> includedDecMatchPattern = rawClassLines.Where(accessModLine => includeClauseRegex.IsMatch(accessModLine)).ToList();
            string namespaceDeclarationLine = rawClassLines.Where(accessModLine => namespaceDeclarationRegex.IsMatch(accessModLine)).FirstOrDefault();

            try
            {
                for (int lineNumber = 0; lineNumber < rawClassLines.Length; lineNumber++)
                {
                    if (rawClassLines[lineNumber].StartsWith("using"))
                    {
                        inputClass.IncludedNamespaces.Add(rawClassLines[lineNumber].Replace("using ", String.Empty));
                    } 
                    Enum.GetNames(typeof(CPrimitiveTypes));       

                    if (Regex.IsMatch(rawClassLines[lineNumber], accessMatch + " " + typeMatch))
                    {
                        typeLineMatches.Add(rawClassLines[lineNumber]);
                    }
                }
            }catch(Exception e)
            {
                string test = e.Message;
            }

               
            
        }

        public void ReturnClassFile(int classFileID)
        {
            
        }
    }
}
